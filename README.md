# Password_Generator
https://smartguar-d.herokuapp.com/
Single page application helps you to create strong password.

# About 
Single page application with which you can create a strong password or check your password for strength, you will also find there useful advice on creating a strong password and how to keep your password safe.

# Download & Setup Instructions

* 1 - Clone project: git clone https://github.com/pavel_samkov/password_generator/
* 2 - cd password_generator
* 3 - Create virtual environment: virtualenv myenv
* 4 - myenv\scripts\activate
* 5 - pip install -r requirements.txt
* 6 - python manage.py runserver

